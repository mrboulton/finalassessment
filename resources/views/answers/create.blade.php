@extends('layouts.app')

@section('content')

<h1 class="header">Add Options</h1>
<!--this brings up the option to input the question title along with selecting the type of question the user wants-->
@if(isset($question))
    <div class="form-back">
        <h4 width="150">{{$question->title}}</h4>
<!-- if user clicks on text option they will be taken to a view of the question-->
            @if($question->question_type == 2)
                <p>Enter Answer Here:</p>
                <input type="text">
<!-- If user selects multiple choice option they will be able to enter the answer they can to submit-->
            @elseif($question->question_type == 1)
                <div class="form-group">
                    {!! Form::open(['url' => 'answers']) !!}
                    {!! Form::label('answer', 'Answer Option:') !!}
                    {!! Form::text('answer', null, ['class' => 'form-control']) !!}
<!-- hidden fields to bring through the ids from the database-->
                    {!! Form::hidden('survey_id', $survey->id) !!}
                    {!! Form::hidden('user_id', $user->id) !!}
                    {!! Form::hidden('question_id', $question->id) !!}
                    {!! Form::submit('Add Answer', ['class' => 'main-button',]) !!}
                    {!! Form::close() !!}
                </div>
            @endif
    </div>
@endif
<!-- This foreach statement pastes the answer onto the view so the user can see them-->
<div class="form-back">
    <h4 width="150">Answer Options</h4>
        @foreach ($answers as $answer)
            <input type="radio" name="bev" value="no">{{ $answer->answer }}<BR>
        @endforeach
<!-- This link thats the user back to the question creation page -->
    <a class="main-button button-float" href="http://localhost:8000/question/create/{{$question->survey->id}}">Complete</a>
</div>

@stop


