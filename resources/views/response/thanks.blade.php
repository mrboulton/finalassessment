@extends('layouts.app')

<link rel="stylesheet" href="/css/app.css" />

@section('content')
<!-- Informs the user that the survey is over and give them the chance to take another survey-->
<h1 class="header">Thank You</h1>
    <h3 class="header">For Taking Part In Our Survey</h3>
    <div class="margins">
<!-- the p object is to ensure that the button is center-->
        <p id="button-pos" class="header">
            <a class="secondary-button" href="http://localhost:8000/response">Take Another Survey</a>
        </p>
    </div>

@endsection