
@extends('layouts.app')

<link rel="stylesheet" href="/css/app.css" />

@section('content')

<h1 class="header">Take Surveys</h1>

<!-- table to inform what the data on the screen is shows for the user-->
<table class="survey-back">
    <thead>
        <tr>
            <th width="250">Title</th>
            <th width="410">Description</th>
        </tr>
    </thead>
</table>
<!-- prints the surveys title and description-->
@if(isset($surveys))
    @foreach($surveys as $survey)
        <table class="survey-back">
            <tbody>
                <tr>
                    <th width="250" height="100">{{$survey->title}}</th>
                    <th width="250">{{$survey->description}}</th>
<!-- take the user to the answers page connected to the survey they have clicked on-->
                    <th><a class="main-button" href="/response/{{$survey->id}}">Take Survey</a></th>
                </tr>
            </tbody>
        </table>
    @endforeach
@endif

@endsection