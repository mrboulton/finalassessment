@extends('layouts.app')

<link rel="stylesheet" href="/css/app.css" />

@section('content')
<!-- this displays the title of the survey-->
<h1 class="header">{{$question->survey->title}}</h1>

<!-- opens form in order for the user to input data-->
{!! Form::open(['url' => 'response']) !!}
<!-- hidden fields ensure that the ids of the question and survey are being pulled through-->
    {!! Form::hidden('question_id', $question->id) !!}
    {!! Form::hidden('survey_id', $question->survey_id) !!}
    <div class="form-back size">
<!-- prints the question title-->
        <h2><strong>{{$question->title}}</strong></h2>
<!--displays the answer that the users can pick, refreshes when user clicks submit answer-->
        @foreach ($question->answer as $answer)
            {!! Form::radio('answer_id', $answer->id) !!} {{$answer->answer}}<br>
        @endforeach
        <input class="main-button button-float" type="submit" name="submit" value="Submit Answer" />
    </div>
{!! Form::close() !!}

@endsection