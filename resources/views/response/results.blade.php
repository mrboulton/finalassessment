@extends('layouts.app')

<link rel="stylesheet" href="/css/app.css" />

@section('content')

<h1 class="header">Results</h1>
<!-- for each question connected to the survey a table is being displayed-->
@if(isset($questions))
    @foreach($questions as $question)
        <h4 class="header">{{$question->title}}</h4>
        <table class="margins">
            <thead>
                <tr>
                    <td>Answers</td>
                    <td>Number of Responses</td>
                </tr>
            </thead>
            <tbody>
<!-- bring the answer through in order for user to view how many people have taken part in their survey-->
        @foreach($question->answer as $answer)
<!-- this counts up the number of clicks each answer received-->
            <?php $singleCount = count($answer->response); ?>
                <tr>
                    <td>{{ $answer->answer }}</td>
                    <td>{{ $singleCount }}</td>
                </tr>
        @endforeach
<!-- shows the total number of responses for each question-->
            <?php $responseCount = count($question->response); ?>
                <tr>
                    <td>Total Number Of Responses:</td>
                    <td><strong>{{ $responseCount }}</strong></td>
                </tr>
            </tbody>
        </table>
    @endforeach
@endif

@endsection


