<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Survey 23045490</title>

    <link rel="stylesheet" href="/css/app.css" />

</head>

<body class="background-colour">
    <nav class="top-bar">
        <div class="container">
            <div id="navbar">
                <!-- Branding Image -->
                <a class="navbar-brand margin-bottom 10px" href="{{ url('/') }}">
                    Survey 23054590
                </a>
                <ul>
<!-- Authentication Links for when user is no logged in -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/response') }}">Take Surveys</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                        <li><a href="{{ url('/login') }}">Login</a></li>
<!-- Links for when the user is logged in to ensure movement around the site-->
                    @else
                        <li>
                            <ul role="menu">
                                <li><a href="{{ url('/response') }}">Take Surveys</a></li>
                                <li><a href="{{ url('/survey/create') }}">Create Survey</a></li>
                                <li><a href="{{ url('/survey') }}">Your Surveys</a></li>
                                <li><a href="{{ url('/logout') }}">{{ Auth::user()->name }}<i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
<!-- as this is the header of everypage this yeild calls upon the content of each page-->
@yield('content')

</body>
</html>
