@extends('layouts.app')

@section('content')

<h1 class="header">Add Question</h1>

{!! Form::open(['url' => 'question']) !!}
    <form>
<!--gives the user the option to enter the title and choose what type of question they want-->
        <div class="form-back">
            {!! Form::label('title', 'Question Name:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
<!-- give the user an option of two to choose from in the array-->
            {!! Form::label('question_type', 'Choose your question type:') !!}
            {!! Form::select('question_type', [1 => "Multiple Choose", 2 => "Text"], ['class' => 'form-control']) !!}
<!-- hidden fields to drag through the ids of the survey and user-->
            {!! Form::hidden('survey_id', $survey->id) !!}
            {!! Form::hidden('user_id', $user->id) !!}
            {!! Form::submit('Next Step', ['class' => 'main-button form-control']) !!}
        </div>
    </form>
{!! Form::close() !!}

<!-- Calls upon the question table to print the title and also drag through the answer options-->
@if(isset($questions))
    @foreach($questions as $question)
        <div class="large-6 columns">
            <div class="answers-back">
                <h4>{{$question->title}}</h4>
<!-- bring the answer through in order for user to view their questions fully-->
                @foreach($question->answer as $answer)
                    <input type="radio" name="bev" value="no">{{ $answer->answer }}<BR>
                @endforeach
            </div>
        </div>
    @endforeach
@endif

@stop