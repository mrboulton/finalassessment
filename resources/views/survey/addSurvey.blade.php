@extends('layouts.app')

<link rel="stylesheet" href="/css/app.css" />

@section('content')

<h1 class="header">Start Your Survey</h1>
    <div class="container form-back">
        <article class="row ">
<!-- brings up a form for the user to enter the title and description of their survey-->
            {!! Form::open(['url' => 'survey']) !!}
            <form>
                <div class="large-9 small-9 columns form-size">
                    {!! Form::label('title', 'Title:') !!}
                    {!! Form::text('title', null, array('placeholder'=>'Enter the title of your survey'), ['class' => 'form-control']) !!}
                </div>
                <div class="large-9 small-9 columns form-size">
                    {!! Form::label('description', 'Description:') !!}
                    {!! Form::textarea('description', null, array('placeholder'=>'Please leave a brief description of what your survey is about'), ['class' => 'form-control']) !!}
                </div>
<!-- hidden field to pull through the user id-->
                    {!! Form::hidden('user_id', $user->id) !!}
                <div class=" large-9 small-9 columns">
 <!-- when the user submits their title and description gets passed to the database which will then be shown on survey page-->
                    {!! Form::submit('Create Survey', ['class' => 'main-button',]) !!}
                </div>
            </form>
            {!! Form::close() !!}
        </article>
    </div>
@stop