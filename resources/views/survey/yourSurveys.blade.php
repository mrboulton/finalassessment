@extends('layouts.app')

<link rel="stylesheet" href="/css/app.css" />

@section('content')

<h1 class="header">Your Surveys</h1>

<!-- sets up a tbale so the user can easily understand what each section means-->
<table class="survey-back">
    <thead>
        <tr>
            <th width="200">Title</th>
            <th width="250">Description</th>
            <th width="150">Edit Survey</th>
            <th width="150">Results</th>
            <th width="150">Delete</th>
        </tr>
    </thead>
</table>
<!-- brings through the title and description from the survey table-->
@if(isset($surveys))
    @foreach($surveys as $survey)
        <table class="survey-back">
            <tbody>
                <tr>
                    <th width="200">{{$survey->title}}</th>
                    <th id="border" width="250">{{$survey->description}}</th>
<!-- these are icons are from fontawesome to allow the user to navigate their surveys-->
                    <td id="border" width="150"><a href="/question/create/{{$survey->id}}"><i class="icon-colour icon-placement fa fa-cog fa-3x" aria-hidden="true"></i></a></td>
                    <td id="border" width="150"><a href="response/results/{{$survey->id}}"><i class="icon-colour icon-placement fa fa-line-chart fa-3x" aria-hidden="true"></i></a></td>
                    <td id="border" width="150">
<!-- this form is to allow the user to delete their selected survey-->
                        {!! Form::open(['method' => 'delete', 'route' => ['survey.destroy', $survey->id]]) !!}
                        {!! Form::button('<i class="icon-colour fa fa-times fa-3x"></i>', ['type'=>'submit', 'title'=>'Delete Survey']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            </tbody>
        </table>
    @endforeach
@endif

@endsection





