<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
//This ensures that the attributes can be put in the database at the same time
//makes them mass assignable
    protected $fillable = ['title', 'question_type', 'user_id', 'survey_id'];

//This is to ensure that the question has many answer
    public function answer() {
        return $this->hasMany('App\Answer');
    }
//This is to ensure that the question belongs to a survey
    public function survey() {
        return $this->belongsTo('App\Survey');
    }
//This is to ensure that the question belongs to a user
    public function user() {
        return $this->belongsTo('App\User');
    }
//This is to ensure that the question has many responses
    public function response() {
        return $this->hasMany('App\Response');
    }

}

