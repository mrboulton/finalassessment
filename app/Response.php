<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Response extends Model
{
//This ensures that the attributes can be put in the database at the same time
//makes them mass assignable
    protected $fillable = ['question_id', 'answer_id'];
    Protected $table = 'responses';


//This is to ensure that the response belongs to a user
    public function user() {
        return $this->belongsTo(User::class);
    }
//This is to ensure that the response belongs to a question
    public function question() {
        return $this->belongsTo('App\Question');
    }

//this is to ensure that response belongs to a answer
    public function answers() {
        return $this->belongsTo('App\Answer');
    }
}
