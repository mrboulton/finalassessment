<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
//This ensures that the attributes can be put in the database at the same time
//makes them mass assignable
    protected $fillable = ['title', 'description', 'user_id'];

//This is to ensure that the survey belongs to a user
    public function user() {
        return $this->belongsTo(User::class);
    }

//This is to ensure that one survey has many questions connected to it
    public function question() {
        return $this->hasMany('App\question');
    }

//this is to ensure that one survey can have many answers
    public function answers() {
        return $this->hasMany(Answer::class);
    }
}
