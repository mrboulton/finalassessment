<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
//This ensures that the attributes can be put in the database at the same time
//makes them mass assignable
    protected $fillable = ['answer', 'question_type', 'user_id', 'survey_id', 'question_id'];

//This is to ensure that the answers belongs to a question
    public function question() {
        return $this->belongsTo('App\Question');
    }

//This is to ensure that the answers belongs to a survey
    public function survey() {
        return $this->belongsTo('App\Survey');
    }

//This is to ensure that the questions belongs to a user
    public function user() {
        return $this->belongsTo('App\User');
    }

//This is to ensure that the answer can have many responses
    public function response() {
        return $this->hasMany('App\Response');
    }
}
