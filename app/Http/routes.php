<?php

// ===============================================
// STATIC PAGES ==================================
// ===============================================

//These routes connect the views to the controller meaning that the pages can be viewed on the web
Route::group(['middlewareGroups' => ['web']], function() {

    Route::resource('/', 'PublicController@index');

//These routes are for the purpose of creating the questions
// and the pages that are involved in that process
    Route::resource("/survey", 'SurveyController');
    Route::resource("/question", 'QuestionController');
    Route::resource("/question/create", 'QuestionController@create');
//These routes are connected to the answers so the user can create answers for the questions
    Route::resource("/answers/create", 'AnswerController@create');
    Route::resource("/answers", 'AnswerController');
//These routes are for the user to repsonse to other peoples surveys
    Route::resource("/response", 'PublicController');
    Route::resource("/response/respond", 'PublicController@create');
    Route::resource("/response/results", 'PublicController@edit');
    Route::resource("/thanks", 'PublicController@destroy');

//This route is for the login, determines if the user is logged in or not
    Route::auth();

    Route::get('/home', 'PublicController@index'); // get for public
});
