<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//These use statements allow the models listed below to be connected to this controller.
//It was deigned like this to ensure all the data can be received easily
use App\Survey;
use Auth;
use App\Http\Requests;

class SurveyController extends Controller
{

    //this function is to ensure that the user is logged in and authorised to access these pages
    public function __construct()
    {
        $this->middleware('auth');
    }

//this function allows the survey to pull the user id through so then the surveys are related to the users
    public function index()
    {
        $user = Auth::user();
//Looks through the survey table to find the user id
        $surveys = Survey::where('user_id', $user->id)->get();
        return view('survey/yourSurveys', ['surveys' => $surveys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//this function returns the view of the survey creater so the user can put information in
    public function create()
    {
        $user = Auth::user();
        return view('survey/addSurvey', ['user' => $user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
// this function requests all data from the input so the user can fill in ths urvey information
    public function store(Request $request)
    {
        $input = $request->all();

        Survey::create($input);
 //pulls the title from the survey and places them in order
        $survey = Survey::where('title', $request->title)->first();
//directs the user to the survey page
        return redirect("question/create/$survey->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//this function is to give the user the ability to delete the surveys after they are created
    public function destroy($id)
    {
        $survey=Survey::findOrFail($id);
//delete the survey and everything related to it from the database
        $survey->delete();
        return redirect ("survey");
    }



}