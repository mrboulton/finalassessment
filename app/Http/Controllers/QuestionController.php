<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
//These use statements allow the models listed below to be connected to this controller.
//It was deigned like this to ensure all the data can be received easily
use App\Question;
use App\Survey;
use Auth;
use App\Http\Requests;

class QuestionController extends Controller
{
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
// user has to be logged in and give them the option to create questions
    public function create($id)
    {
//make sure user is logged in
        $user = Auth::user();
//find the id in the survey table
        $survey = Survey::findOrFail($id);
        $questions = Question::where('survey_id', $id)->get();
//returns the user to question creation and brings all the ids needed
        return view('questions/questionCreation', ['user' => $user, 'survey' => $survey, 'questions' => $questions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
//this function is to store the data in questions table then allow the user to create answers
    public function store(Request $request)
    {
//allows the user to input data
        $input = $request->all();
        Question::create($input);
 //pull through the questions in order
        $question = Question::where('title', $request->title)->first();
        return redirect("answers/create/$question->id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    //this function is to show the question
    public function show($id)
    {
        $question = Question::findOrFail($id);
        return view("", ['question' => $question]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }




}
