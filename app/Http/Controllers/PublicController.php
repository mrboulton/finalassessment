<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
//These use statements allow the models listed below to be connected to this controller.
//It was deigned like this to ensure all the data can be received easily
use Auth;
use App\Survey;
use App\Question;
use App\Answer;
use App\Response;
use App\Http\Requests;

class PublicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

//this function allows the system to collect all the survey data
//then redirects the user to the taking surveys page
////no need to be logged in this is public
    public function index()
    {
        $surveys = Survey::all();
        return view('response/take', ['surveys' => $surveys]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
//finds the question from the question form so the user can access them to answer.
    public function create($id)
    {
        $question = Question::findOrFail($id);
        return view('response/respond', ['question' => $question]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

//this function allows the data that the user inputs to be sent to the response database
// while the user to directed to the end thankyou page
    public function store(Request $request)
    {
        $input = $request->all();
//allows data to be created iun the response model and database
        Response::create($input);
// pulls the id from the survey then first question id for the question to be viewed in order
        $question = Question::where('survey_id', $request->survey_id)->where('id', '>', $request->question_id)->first();
// if there is another question user get ot answer that
        if (isset($question)) {
            return redirect("response/respond/$question->id");
//if there are no question left user gets directed to the thankyou page
        } else {
            return redirect("thanks");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//this function is to show the results on the results page once the user has completed the survey
    public function show($id)
    {
        $survey = Survey::findOrFail($id);
 //order the reulsts in order starting with the first
        $question = Question::where('survey_id', $survey->id)->OrderBy('id','asc')->first();
 //take user back to the respond page
        return redirect("response/respond/$question->id");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//this function is for count the repsonses and printing them on the results page
    public function edit($id)
    {
//this allows us to count the repsonses from the database
        Response::count();
//find the survey id
        $survey = Survey::findOrFail($id);
        $questions= Question::where('survey_id', $survey->id)->get();
//allows counting on the results page
        $count = Response::count();
        return view("response/results", ['count'=> $count, 'questions' => $questions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//returns the user to the thankyou page
    public function destroy()
    {
        return view('response.thanks');
    }
}
