<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Answer;
use App\Question;
use App\Survey;
use Auth;
use App\Http\Requests;

class AnswerController extends Controller
{

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
// This block of code is to ensure that the user is logged in,
// Once this is done then the ids are collected from the database in order for the user to create their answers
        $user = Auth::user();
        $question = Question::findOrFail($id);
//find the survey id to bring to the answers table
        $survey = Survey::findOrFail($question->survey_id);
        $answers = Answer::where('question_id', $id)->get();
 //take user to the create page while to brings all the ids from the table so the right answers are getting shown
        return view('answers/create', ['user' => $user, 'question' => $question, 'survey'=>$survey, 'answers' => $answers]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //requests all data from answers create in order to redirect them onto another page
        $input = $request->all();

        Answer::create($input);

        return redirect("answers/create/$request->question_id");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
