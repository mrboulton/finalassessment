<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('questions')->insert([
            ['id' => 1, 'survey_id' => 1, 'user_id' => 1, 'title' => "Question 1 Test", 'option_name' => "Forgot what this is"],
        ]);
    }
}
