<?php

use Illuminate\Database\Seeder;

class SurveyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('surveys')->insert([
            ['id' => 1, 'title' => "SurveyTest1", 'user_id' => 1, 'description' => "tetsing to see if I can call data from the database"],
            ['id' => 2, 'title' => "Survey Test 2", 'user_id' => 1, 'description' => "please please please work"],
          ]);
    }
}
