<?php

use Illuminate\Database\Seeder;

class AnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('answers')->insert([
            ['id' => 1,
             'user_id' => 1,
              'question_id' => 1,
              'survey_id' => 1,
              'answer' => 'test answer'],
        ]);
    }
}
