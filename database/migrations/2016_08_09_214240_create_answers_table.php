<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
//creates a new id for this table which can be called from other tables to connect them
            $table->increments('id');
//brings through all the id from other table so they all link together
//This to done for data cna be passed around much easier
            $table->integer('user_id');
            $table->integer('question_id');
            $table->integer('survey_id');
            $table->string('answer');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('answers');
    }
}
